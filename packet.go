// Package r30x packet definitions
package r30x

import (
	"bytes"
)

// Packet -
type Packet struct {
	Head   [2]byte
	Addr   [4]byte
	PID    byte
	Length [2]byte
	Data   []byte
	CRC    [2]byte
}

// CRCCalc -
func (p *Packet) CRCCalc() [2]byte {
	b := bytes.NewBuffer([]byte{p.PID})
	b.Write(p.Length[0:1])
	b.Write(p.Data)
	return crcCalc(b.Bytes())
}

// GetLen -
func (p *Packet) GetLen() [2]byte {
	return getLen(p.Data)
}

// RLen - Получить размер данных пакета (int)
func (p *Packet) RLen() int {
	return len(p.Data) + 11
}

func parsePacket(d []byte) (p *Packet, err error) {
	const f = "parsePacket"
	if len(d) < 12 {
		//		r._err(f, "Too small data.")
		return nil, ErrSmallData
	}
	p = &Packet{}
	p.Head = [2]byte{d[0], d[1]}
	p.Addr = [4]byte{d[2], d[3], d[4], d[5]}
	p.PID = d[6]
	p.Length = [2]byte{d[7], d[8]}
	ln := len2int(p.Length)
	if len(d) < ln+8 {
		//		r._err(f, "Too small data (%d), expected %d", len(d), ln+8)
		return nil, ErrSmallData
	}
	p.Data = d[8 : ln-2]
	p.CRC = [2]byte{d[8+ln-3], d[8+ln-2]}
	//= проверка корректности пакета >
	// CRC
	t := []byte{p.PID, p.Length[0], p.Length[1]}
	t = append(t, p.Data...)
	crc := crcCalc(t)
	if !bytes.Equal(crc[0:1], p.CRC[0:1]) {
		return nil, ErrCRCError
	}
	//< проверка корректности пакета =
	return p, nil
}
