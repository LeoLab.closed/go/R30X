// Package r30x Debugging
package r30x

import (
	"fmt"

	"gitlab.com/LeoLab/go/leotools"
)

// SetDbg -
func (r *R30X) SetDbg(d bool) (err error) {
	err = nil
	if d {
		if r.log == nil {
			if r.log, err = leotools.NewLogger(&leotools.LogConf{File: r.cfg.LogFile, Name: r.cfg.LogName, RCDepth: 3}); err != nil {
				r.log = nil
			}
		}
	} else {
		if r.log != nil {
			r.log.Info("Finish DBG.")
			err = r.log.Close()
			r.log = nil
		}
	}
	return
}

func (r *R30X) _dbg(f, m string, v ...interface{}) {
	if r.log != nil && r.cfg.LogLevel > 4 {
		r.log.Debug(_ms(f, m, v...))
	}
}

func (r *R30X) _info(f, m string, v ...interface{}) {
	if r.log != nil && r.cfg.LogLevel > 3 {
		r.log.Info(_ms(f, m, v...))
	}
}

func (r *R30X) _note(f, m string, v ...interface{}) {
	if r.log != nil && r.cfg.LogLevel > 2 {
		r.log.Info(_ms(f, m, v...))
	}
}

func (r *R30X) _warn(f, m string, v ...interface{}) {
	if r.log != nil && r.cfg.LogLevel > 1 {
		r.log.Warning(_ms(f, m, v...))
	}
}

func (r *R30X) _err(f, m string, v ...interface{}) error {
	if r.log != nil && r.cfg.LogLevel > 0 {
		r.log.Err(_ms(f, m, v...))
	}
	return fmt.Errorf(m, v...)
}

func (r *R30X) _crit(f, m string, v ...interface{}) error {
	if r.log != nil {
		r.log.Crit(_ms(f, m, v...))
	}
	return fmt.Errorf(m, v...)
}

func _ms(f, m string, v ...interface{}) string {
	return "[R30X." + f + "]" + fmt.Sprintf(m, v...)
}
