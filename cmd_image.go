// Package r30x cmd_image
/*
	Команды работы с отпечатками
*/
package r30x

import (
	"time"
)

// UpImage Загрузить отпечаток из устройства
func (r *R30X) UpImage() (data []byte, err error) {
	const f = "UpImage"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x0A}
	err = r.writeCmd(cmd)
	if err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return nil, err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readAck()
	if err != nil {
		r._err(f, "Error reading answer: %s", err.Error())
		return nil, err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer.")
		return nil, ErrEmptyAnswer
	}
	if err := checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return nil, err
	}
	r._dbg(f, "Ready to transfer")
	time.Sleep(r.cfg.ReadDelay)
	data, err = r.readData()
	if err != nil {
		r._err(f, "Error receiving data: %s", err.Error())
		return nil, err
	}
	r._info(f, "Received data length: %d", len(data))
	return data, nil
}

// DownImage Загрузить отпечаток в устройство
func (r *R30X) DownImage(data []byte) (err error) {
	const f = "DownImage"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x0B}
	err = r.writeCmd(cmd)
	if err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readAck()
	if err != nil {
		r._err(f, "Error reading answer: %s", err.Error())
		return err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer")
		return ErrEmptyAnswer
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._dbg(f, "Ready to transfer")
	err = r.writeData(data)
	if err != nil {
		r._err(f, "Error send data: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}

// Img2Tz -
func (r *R30X) Img2Tz(num byte) (err error) {
	const f = "Img2Tz"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()

	cmd := []byte{0x02, num}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer")
		return ErrEmptyAnswer
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}
