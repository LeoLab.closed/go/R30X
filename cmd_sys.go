// Package r30x - system  and service commands
/*
	Системные и сервисные команды
*/
package r30x

import (
	"time"
)

// ReadSysPara -
/*
Возвращаемые данные:
	[0] [2]byte, Status register
	[2] [2]byte, System identifier code = 0x0009
	[4] [2]byte, Finger library size
	[6] [2]byte, Security level
	[8] [4]byte, Device address
	[12] [2]byte, Data packet size (0..3)
	[14] [2]byte, Baud settings, N*9600
*/
func (r *R30X) ReadSysPara() (ret []byte, err error) {
	const f = "ReadSysPara"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x0F}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return nil, err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()

	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return nil, err
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return nil, err
	}
	r._info(f, "Success")
	return ack[1:], nil
}

// SetSysPara - Установить параметр
func (r *R30X) SetSysPara(n int, val byte) (err error) {
	const f = "SetSysPara"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()

	cmd := []byte{0x0E, byte(n), val}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}
