// Package r30x writing data
package r30x

import (
	"bytes"
)

func (r *R30X) writeCmd(d []byte) (err error) {
	const f = "writeCmd"
	cp := Packet{
		Head: [2]byte{0xEF, 0x01},
		Addr: r.cfg.Address,
		PID:  0x01,
		Data: d,
	}
	cp.Length = cp.GetLen()
	cp.CRC = cp.CRCCalc()
	if err := r.write([]Packet{cp}); err != nil {
		r._err(f, "Error: (%T) %s", err, err.Error())
		return err
	}
	return nil
}

func (r *R30X) writeData(d []byte) (err error) {
	const f = "writeData"
	var pid byte = 0x02
	pkgs := make([]Packet, 0)
	fLen := len(d)
	dc := fLen / r.cfg.PkgSize
	for i := 0; i < dc; i++ {
		pkg := Packet{
			Head: [2]byte{0xEF, 0x01},
			Addr: r.cfg.Address,
			PID:  pid,
			Data: d[i*r.cfg.PkgSize : i*r.cfg.PkgSize+r.cfg.PkgSize],
		}
		pkg.Length = pkg.GetLen()
		pkg.CRC = pkg.CRCCalc()
		pkgs = append(pkgs, pkg)
		if i == dc-1 {
			pid = 0x08
		}
	}
	if err := r.write(pkgs); err != nil {
		r._err(f, "Error: %s", err.Error())
		return err
	}
	return nil
}

func (r *R30X) write(pkgs []Packet) (err error) {
	const f = "write"
	r._dbg(f, "Enter with %d packets", len(pkgs))
	if r.ser == nil {
		r._err(f, "Try write to closed port!")
		return ErrPortClosed
	}
	defer func() { r._dbg(f, "Leave") }()

	data := bytes.NewBuffer([]byte{})
	for _, p := range pkgs {
		data.Reset()
		data.Write(p.Head[0:])
		data.Write(p.Addr[0:])
		data.Write([]byte{p.PID})
		data.Write(p.Length[0:])
		data.Write(p.Data)
		data.Write(p.CRC[0:])
		wb, err := r.ser.Write(data.Bytes())
		if err != nil {
			r._err(f, "Error write data (%T) %s", err, err.Error())
			return err
		}
		r._info(f, "Writed %d bytes", wb)
	}
	return nil
}
