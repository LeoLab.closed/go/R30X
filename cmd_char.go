package r30x

import (
	"time"
)

// UpChar - выгрузить с устройства
func (r *R30X) UpChar(num byte) (data []byte, err error) {
	const f = "UpChar"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() { r._dbg(f, "Leave"); r.mtx.Unlock() }()

	cmd := []byte{0x08, num}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return nil, err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return nil, err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer")
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return nil, err
	}
	data, err = r.readData()
	if err != nil {
		r._err(f, "Error reading data: %s", err.Error())
		return nil, err
	}
	r._info(f, "Received data length: %d", len(data))
	return data, nil
}
