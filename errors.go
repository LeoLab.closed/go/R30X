// Package r30x errors definitions
package r30x

import (
	"errors"
	"fmt"
)

// ErrPortClosed -
var ErrPortClosed = errors.New("Port closed")

// ErrInvalidHeader -
var ErrInvalidHeader = errors.New("Invalid packet header")

// ErrCRCError -
var ErrCRCError = errors.New("CRC Error")

// ErrEmptyResponce -
var ErrEmptyResponce = errors.New("Empty responce")

// ErrIncorrectPacketType -
var ErrIncorrectPacketType = errors.New("Incorrect packet type")

// ErrSmallData -
var ErrSmallData = errors.New("Too small readed data")

// ErrEmptyAnswer -
var ErrEmptyAnswer = errors.New("Empty answer")

// ErrUnexpectedAckCode -
type ErrUnexpectedAckCode struct {
	code byte
}

func newEUnexpectedAckCode(c byte) *ErrUnexpectedAckCode {
	return &ErrUnexpectedAckCode{code: c}
}

func (e *ErrUnexpectedAckCode) Error() string {
	return fmt.Sprintf("Unexpected ack code: %X", e.code)
}

func checkAck(f string, ackCode byte) error {
	f = f + ".Result"
	switch ackCode {
	case 0x00:
		return nil
	case 0x01:
		return errors.New("Ack(01): Error when receiving data package")
	case 0x02:
		return errors.New("Ack(02): No finger on the sensor")
	case 0x03:
		return errors.New("Ack(03): Fail to enroll the finger")
	case 0x06:
		return errors.New("Ack(06): Fail to generate character file due to the over-disorderly fingerprint image")
	case 0x07:
		return errors.New("Ack(07): Fail to generate character file due to lackness of character point or over-smallness of fingerprint image")
	case 0x08:
		return errors.New("Ack(08): Finger doesn’t match")
	case 0x09:
		return errors.New("Ack(09): Fail to find the matching finger")
	case 0x0A:
		return errors.New("Ack(0A): Fail to combine the character files")
	case 0x0B:
		return errors.New("Ack(0B): Addressing PageID is beyond the finger library")
	case 0x0C:
		return errors.New("Ack(0C): Error when reading template from library or the template is invalid")
	case 0x0D:
		return errors.New("Ack(0D): Error when uploading template")
	case 0x0E:
		return errors.New("Ack(0E): Module can’t receive the following data packages")
	case 0x0F:
		return errors.New("Ack(0F): Error when uploading image")
	case 0x10:
		return errors.New("Ack(10): Fail to delete the template")
	case 0x11:
		return errors.New("Ack(11): Fail to clear finger library")
	case 0x13:
		return errors.New("Ack(13): Wrong password")
	case 0x15:
		return errors.New("Ack(15): Fail to generate the image for the lackness of valid primary image")
	case 0x18:
		return errors.New("Ack(18): Error when writing flash")
	case 0x19:
		return errors.New("Ack(19): No definition error")
	case 0x1A:
		return errors.New("Ack(1A): Invalid register number")
	case 0x1B:
		return errors.New("Ack(1B): Incorrect configuration of register")
	case 0x1C:
		return errors.New("Ack(1C): Wrong notepad page number")
	case 0x1D:
		return errors.New("Ack(1D): Fail to operate the communication port")
	default:
		return &ErrUnexpectedAckCode{code: ackCode}
	}
}
