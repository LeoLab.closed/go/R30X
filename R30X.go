// Package r30x project R30X.go
package r30x

import (
	"sync"

	"github.com/tarm/serial"
	"gitlab.com/LeoLab/go/leotools"
)

// Константы версии
const (
	Version = "0.0.1"
	Date    = "04/07/2018"
	MName   = "UWB/R30X"
)

// R30X -
type R30X struct {
	cfg  *Config
	mtx  *sync.Mutex
	ser  *serial.Port
	ubuf []byte //Буфер недоразобранных данных.

	log *leotools.Logger
}

// New -
func New(c *Config) (R *R30X, err error) {
	if c == nil {
		c = GetDefaults()
	}
	R = &R30X{
		cfg: c,
	}
	return
}

// Open -
func (r *R30X) Open() (err error) {
	const f = "Open"
	r._dbg(f, "Enter")
	defer func() { r._dbg(f, "Leave") }()

	r.ser, err = serial.OpenPort(&serial.Config{Name: r.cfg.TTY, Baud: r.cfg.BaudRate, ReadTimeout: r.cfg.TimeOut})
	if err != nil {
		return r._err(f, "Ошибка открытия порта: %s", err, err.Error())
	}
	r.mtx = &sync.Mutex{}

	r.ubuf = make([]byte, 0)

	return nil
}

// Close -
func (r *R30X) Close() (err error) {
	const f = "Close"
	r._dbg(f, "Enter")
	defer func() { r._dbg(f, "Leave") }()

	if r.ser != nil {
		if err = r.ser.Close(); err != nil {
			return r._err(f, "Ошибка закрытия порта: %s", err.Error())
		}
	}
	return nil
}

func crcCalc(d []byte) (r [2]byte) {
	var sum uint16
	for _, t := range d {
		sum += uint16(t)
	}
	r = [2]byte{byte(sum >> 8), byte(sum)}
	return
}

// Получить размер данных
// +2 - размер должен быть вместе с CRC
func getLen(d []byte) (l [2]byte) {
	ln := len(d) + 2
	l[0] = byte(ln >> 8)
	l[1] = byte(ln)
	return
}

func len2int(l [2]byte) int {
	return int(int(l[0])<<8) + int(l[1])
}
