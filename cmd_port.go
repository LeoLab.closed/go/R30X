package r30x

import (
	"time"
)

// Control - включить/выключить USB/UART
func (r *R30X) Control(s bool) (err error) {
	const f = "Control"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x17}
	if s {
		cmd = append(cmd, 0x01)
	} else {
		cmd = append(cmd, 0x00)
	}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}
