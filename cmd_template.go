package r30x

import (
	"time"
)

// TempleteNum -
func (r *R30X) TempleteNum() (tn int, err error) {
	const f = "TempleteNum"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()

	cmd := []byte{0x1D}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return 0, err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return 0, err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer")
		return 0, ErrEmptyAnswer
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return 0, err
	}
	tn = 0
	l := len(ack[1:])
	for i, b := range ack[1:] {
		sh := uint((l - i - 1) * 8)
		tn |= int(b) << sh
	}
	return tn, nil
}

// RegModel -
func (r *R30X) RegModel() (err error) {
	const f = "RegModel"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()

	cmd := []byte{0x05}
	if err = r.writeCmd(cmd); err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._dbg(f, "Error reading answer: %s", err.Error())
		return err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer")
		return ErrEmptyAnswer
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}
