// Package r30x cmd_passwd
/*
	Команды работы с паролями
*/
package r30x

import (
	"time"
)

// VfyPwd Проверить пароль
func (r *R30X) VfyPwd(p [4]byte) error {
	const f = "VfyPwd"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x13, p[0], p[1], p[2], p[3]}
	err := r.writeCmd(cmd)
	if err != nil {
		r._err(f, "Error send command: %s ", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer.")
		return ErrEmptyAnswer
	}
	if err := checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}

// SetPwd Установить пароль
func (r *R30X) SetPwd(p [4]byte) (err error) {
	const f = "SetPwd"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x12, p[0], p[1], p[2], p[3]}
	err = r.writeCmd(cmd)
	if err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readData()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if err = checkAck(f, ack[0]); err != nil {
		r._err(f, "Received error: %s", err.Error())
		return err
	}
	r._info(f, "Success")
	return nil
}
