// Package r30x package configuration
package r30x

import (
	"time"
)

// Config -
type Config struct {
	TTY       string
	BaudRate  int
	TimeOut   time.Duration
	ReadDelay time.Duration
	Address   [4]byte
	PkgSize   int

	LogFile  string
	LogName  string
	LogLevel int
}

// GetDefaults -
func GetDefaults() (c *Config) {
	c = &Config{
		TTY:       "/dev/ttyS2",
		BaudRate:  57600,
		TimeOut:   time.Second * 5,
		ReadDelay: time.Millisecond * 20,
		Address:   [4]byte{0xFF, 0xFF, 0xFF, 0xFF},
		PkgSize:   128, //0 = 32,1 = 64,2 = 128,3 = 256

		LogFile:  "",
		LogName:  "",
		LogLevel: 2,
	}
	return c
}
