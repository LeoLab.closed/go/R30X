// Package r30x cmd_finger
/*
	Команды работы с "пальчиком".
*/
package r30x

import (
	"time"
)

// GenImg Отсканировать отпечаток и сохранить в ImageBuffer
func (r *R30X) GenImg() error {
	const f = "GetnImg"
	r._dbg(f, "Enter")
	r.mtx.Lock()
	defer func() {
		r._dbg(f, "Leave")
		r.mtx.Unlock()
	}()
	cmd := []byte{0x01}
	err := r.writeCmd(cmd)
	if err != nil {
		r._err(f, "Error send command: %s", err.Error())
		return err
	}
	time.Sleep(r.cfg.ReadDelay)
	ack, err := r.readAck()
	if err != nil {
		r._err(f, "Error read answer: %s", err.Error())
		return err
	}
	if len(ack) < 1 {
		r._err(f, "Empty answer.")
		return ErrEmptyAnswer
	}
	if err := checkAck(f, ack[0]); err != nil {
		return err
	}
	r._info(f, "Success")
	return nil
}
