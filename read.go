// Package r30x reading data
package r30x

func (r *R30X) readAck() (ack []byte, err error) {
	const f = "readAck"
	r._dbg(f, "Enter")
	defer func() {
		r._dbg(f, "Leave")
	}()
	r.ubuf = []byte{}
	rp, err := r.read()
	if err != nil {
		r._err(f, "Error: %s", err, err.Error())
		return nil, err
	}
	if rp.PID != 0x07 {
		r._err(f, "Incorrect packet type (%X)", rp.PID)
		return nil, ErrIncorrectPacketType
	}
	ack = rp.Data
	return ack, nil
}

func (r *R30X) readData() (data []byte, err error) {
	const f = "readData"
	r._dbg(f, "Enter")
	do := true
	data = make([]byte, 0)
	defer func() {
		r._dbg(f, "Leave")
	}()
	for do {
		r._dbg(f, "Reading packet...")
		rp, err := r.read()
		if err != nil {
			r._err(f, "Error: %s", err.Error())
		}
		r._dbg(f, "Readed packet %X", rp.PID)
		for cont := true; cont; cont = (len(r.ubuf) > 0) {
			switch rp.PID {
			case 0x02:
				r._dbg(f, "Received data packet.")
			case 0x08:
				do = false
				r._dbg(f, "Received final packet.")
			default:
				r._err(f, "Incorrect packet type: %X", rp.PID)
				return nil, ErrIncorrectPacketType
			}
			data = append(data, rp.Data...)
		}
	}
	return data, nil
}

func (r *R30X) read() (p *Packet, err error) {
	const f = "read"
	r._dbg(f, "Enter")
	defer func() {
		r._dbg(f, "Leave")
	}()
	if r.ser == nil {
		r._err(f, "Try read from closed port.")
		return nil, ErrPortClosed
	}
	buf := make([]byte, 512)

	rb, err := r.ser.Read(buf)
	if err != nil {
		r._err(f, "Error: (%T) %s", err, err.Error())
		return nil, err
	}
	r._dbg(f, "Readed %d bytes: {%#v}", rb, buf[:rb])
	if rb < 12 {
		r._err(f, "Too small data.")
		return nil, ErrSmallData
	}
	// Непосредственно разбор пакета.
	r.ubuf = append(r.ubuf, buf[:rb]...)
	p, err = parsePacket(r.ubuf)
	if err != nil {
		r._err(f, "Error parse packet: %s", err.Error())
		return nil, err
	}
	r.ubuf = r.ubuf[p.RLen():]
	return p, nil
}

/*
func (r *R30X) reads(d bool) (data []Packet, err error) {
	if log != nil {
		_dbg("[read] Enter")
	}
	if r.ser == nil {
		if log != nil {
			_err("[read] Try read from closed port!")
		}
		return nil, EPortClosed
	}
	do := d
	data = make([]Packet, 0)
	buf := make([]byte, 512)
	var ubuf, pbuf []byte
	r.mtx.Lock()
	defer func() {
		if log != nil {
			_dbg("[read] Leave")
		}
		r.mtx.Unlock()
	}()
	for do {
		rb, err := r.ser.Read(buf)
		if err != nil {
			if log != nil {
				_err("[read] Read error: (%T) %s", err, err.Error())
			}
			return nil, err
		}
		if log != nil {
			_dbg("[read] Readed %d bytes {%#v}", rb, buf)
		}
		pbuf = ubuf
		pbuf = append(pbuf, buf...)
		//= Разбор пакетов >
		ci := 0
		ps := 0
		for ci < rb {
			pkg := &Packet{}
			ps = ci
			if (ci + 10) > rb {
				ubuf = pbuf[ps:rb]
				break
			}
			if !bytes.Equal(pbuf[ci:ci+1], []byte{0xEF, 0x01}) {
				if log != nil {
					_err("[read] Found incorrect packet header: {%#v}", pbuf[ci:ci+1])
				}
				return nil, EInvalidHeader
			}
			pkg.Head = [2]byte{0xEF, 0x01}
			ci = ci + 2
			if !bytes.Equal(pbuf[ci:ci+4], r.cfg.Address[0:3]) {
				if log != nil {
					_err("[read] Incorrect device address: {%#v}", pbuf[ci:ci+4])
				}
			}
			pkg.Addr = r.cfg.Address
			ci = ci + 4
			pkg.PID = pbuf[ci]
			ci = ci + 1
			pkg.Length[0] = pbuf[ci]
			ci = ci + 1
			pkg.Length[1] = pbuf[ci]
			pLen := (int(pkg.Length[0]<<8) | int(pkg.Length[1]))
			if (ci + pLen) > (rb - 1) {
				ubuf = pbuf[ps:rb]
				break
			}
			pkg.Data = pbuf[ci : ci+pLen]
			ci = ci + pLen
			pkg.CRC[0] = pbuf[ci]
			ci = ci + 1
			pkg.CRC[1] = pbuf[ci]
			crc := crcCalc(pbuf[(ci - (pLen + 3)):(ci - 2)])
			if !bytes.Equal(pkg.CRC[0:1], crc[0:1]) {
				if log != nil {
					_err("[read] CRC Error")
				}
				return nil, ECRCError
			}
			ci = ci + 1
			if pkg.PID == 0x08 { // Получили завершающий пакет
				do = false
			}
			data = append(data, *pkg)
		}

		//< Разбор пакетов =
	}
	return data, nil
}
*/
